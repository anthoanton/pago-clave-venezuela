/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, Alert, Modal, FlatList, Switch, Image} from 'react-native';
import { Container, Header, Content, Text,View, Picker, Item, Form, Input, Label, Spinner, Body, Title, Button, Icon, ListItem, Left, Right, CheckBox, List, Tab, Tabs} from 'native-base';
import SmsAndroid  from 'react-native-get-sms-android';
import AsyncStorage from '@react-native-community/async-storage';
import {imagen} from './img.js';
import ViewSms from './viewsms.js';

type Props = {};
var store = [];
export default class App extends Component<Props> {

  constructor(props){
        super(props)
        this.state={
            banco:'',
            tlf:'',
            cedula:'',
            monto:'',
            alias:'',
            spinner:false,
            errorMonto:'',
            check:false,
            modalVisible: false,
            checkVisible: true,
            aliasVisible: false
        } 
    }

  componentWillMount(){
      this.getUserId()  
  }

  handleCheckBox = () => this.setState({ check: !this.state.check });

  saveUserId = async ()=> {

    var store_add ={
      alias:this.state.alias,
      banco: this.state.banco,
      tlf:this.state.tlf,
      cedula:this.state.cedula
    }
    store.push(store_add);
    
    store=JSON.stringify(store);
    try {
      await AsyncStorage.setItem('contactos', store);
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  };

  deleteUserId = async (alias) => {
    try {
      for(var i = 0; i<store.length; i++) {
          if (store[i].alias == alias) {
             store.splice(i, 1);
             break;
          }
      }
      store=JSON.stringify(store);
      await AsyncStorage.setItem('contactos', store);
        
    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }
  };

  getUserId = async () => {
    try {
      store = await AsyncStorage.getItem('contactos') || []

      if(store != ''){
        store=JSON.parse(store);
      }
      console.log(store);

    } catch (error) {
      // Error retrieving data
      console.log(error.message);
    }

    return store;
  }

  validateDecimal(valor) {
    var RE = /[0-9]{1,2}(\,[0]{2})/;
    let res;
    if (RE.test(valor)) {
        res = '';
        this.setState({
          errorMonto:res
        })
    } else {
        res = 'formato del monto ej. 00,00';
        this.setState({
          errorMonto:res
        })
    }
  };

  enviarSms(mensaje) {

    SmsAndroid.autoSend("2662", mensaje,(fail) => {
      this.setState({
        spinner:false
      });
      Alert.alert(
          'No se pudo enviar el Mensaje',
          "Verifique si posee saldo disponible o contacte a su proveedor de servicios.",
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
    },(success) => {
        this.setState({
          spinner:false
        });
        Alert.alert(
          'Mensaje enviado con exito',
          "Revise su bandeja de entrada para verificar la confirmacion del banco.",
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          {cancelable: false},
        );
    });

  }

  render() {

    return (
      <Container>
        <Header style={{backgroundColor: "#E60026"}} hasTabs>
          <Left>
          {
              this.state.alias != ''?
            <Button transparent
              onPress={() => { 
                    this.setState({
                      banco:'',
                      tlf:'',
                      cedula:'',
                      monto:'',
                      alias:'',
                      monto:'',
                      check:false,
                      checkVisible:true,
                      aliasVisible:false

                    }) 
                  }}>
                <Icon type="FontAwesome" name="arrow-left" />
              </Button>:
              <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                              <Image
                                style={{
                                  margin: 0,
                                  width: 50,
                                  height: 50,
                                  resizeMode: 'contain',
                                }}
                                source={{
                                  uri: imagen
                                }}
                              />
                          </View>
            }
          </Left>
          <Body style={{margin: 10}}>
            <Title>Pago Clave</Title>
          </Body>
          <Right>
            <Button transparent
              onPress={() => { 
                    this.setState({
                      modalVisible:true     
                    }) 
                  }}> 
                <Icon type="FontAwesome" name="address-book" />
              </Button>
          </Right>
        </Header>
        <Tabs>
          <Tab heading="Registro" tabStyle={{backgroundColor: '#E60026'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#E60026'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}>
            <Content>
          <Form>
            {
              this.state.aliasVisible == true?
              <View style={{borderColor: 'white',  marginLeft:20, marginRight:20,marginTop:5}}>
              <Label floatingLabel >Alias</Label>
              <Item style={{marginTop:10, marginBottom: 5}}>
              <Icon type="FontAwesome" name="user" style={{color:"#E60026"}}/>
              <Text>{this.state.alias}</Text>
              </Item>
              <Label floatingLabel >Banco</Label>
              <Item style={{marginTop:10, marginBottom: 5}}>
              <Icon type="FontAwesome" name="bank" style={{color:"#E60026"}}/>
              <Text>{this.state.banco}</Text>
              </Item>
              <Label floatingLabel >Telefono</Label>
              <Item style={{marginTop:10, marginBottom: 5}}>
              <Icon type="FontAwesome" name="phone" style={{color:"#E60026"}}/>
              <Text>{this.state.tlf}</Text>
              </Item>
              <Label floatingLabel >Cedula</Label>
              <Item style={{marginTop:10, marginBottom: 5}}>
              <Icon type="FontAwesome" name="id-card" style={{color:"#E60026"}}/>
              <Text>{this.state.cedula}</Text>
              </Item>
              </View>
              :<View style={{borderColor: 'white',  marginLeft:20, marginRight:20,marginTop:5}}>
                <Label floatingLabel >Banco</Label>
                <Item>
                <Icon type="FontAwesome" name="bank" style={{color:"#E60026"}}/>
                  <Picker
                  selectedValue={this.state.banco}
                  onValueChange={(itemValue, itemIndex) =>
                    this.setState({banco: itemValue})
                  }>
                  <Picker.Item label="Seleccione..." value="" />
                  <Picker.Item label="Venezuela 0102" value="0102" />
                  <Picker.Item label="Venezolano de C#E60026ito 0104" value="0104" />
                  <Picker.Item label="Mercantil 0105" value="0105" />
                  <Picker.Item label="Provincial 0108" value="0108" />
                  <Picker.Item label="Bancaribe 0114" value="0114" />
                  <Picker.Item label="Exterior 0115" value="0115" />
                  <Picker.Item label="Occidental de descuento 0116" value="0116" />
                  <Picker.Item label="Banco caroni 0128" value="0128" />
                  <Picker.Item label="Banesco 0134" value="0134" />
                  <Picker.Item label="Banco plaza 0138" value="0138" />
                  <Picker.Item label="BFC Banco fondo comun 0151" value="0151" />
                  <Picker.Item label="100% banco 0156" value="0156" />
                  <Picker.Item label="Del sur 0157" value="0157" />
                  <Picker.Item label="Banco del tesoro 0163" value="0163" />
                  <Picker.Item label="Banco agricola de venezuela 0166" value="0166" />
                  <Picker.Item label="Bancrecer 0168" value="0168" />
                  <Picker.Item label="Mi banco 0169" value="0169" />
                  <Picker.Item label="Bancamiga 0172" value="0172" />
                  <Picker.Item label="Banplus 0174" value="0174" />
                  <Picker.Item label="Bicentenario 0175" value="0175" />
                  <Picker.Item label="Banfanb 0177" value="0177" />
                  <Picker.Item label="BNC nacional de c#E60026ito 0191" value="0191" />
                  </Picker>
                </Item>
                <Label floatingLabel >Movil</Label>
                <Item>
                <Icon type="FontAwesome" name="phone" style={{color:"#E60026"}}/>
                  <Input 
                    keyboardType = 'numeric'
                    maxLength={11}
                    type='text' 
                    value={this.state.tlf}
                    onChangeText={(value) => this.setState({tlf: value})}
                    placeholder='Número de Telefono'
                  />
                </Item>
                <Label floatingLabel >Cedula</Label>
                <Item>
                <Icon type="FontAwesome" name="id-card" style={{color:"#E60026"}}/>
                  <Input 
                    keyboardType = 'numeric'
                    maxLength={8}
                    type='text' 
                    value={this.state.cedula}
                    onChangeText={(value) => this.setState({cedula: value})}
                    placeholder='Cédula de Identidad'
                  />
              </Item>
            </View>
            }
            <View style={{borderColor: 'white',  marginLeft:20, marginRight:20,marginTop:5}}>
            <Label floatingLabel >Monto</Label>
            <Item>
            <Icon type="FontAwesome" name="money" style={{color:"#E60026"}}/>
              <Input 
                keyboardType = 'numeric'
                maxLength={20}
                type='text' 
                value={this.state.monto}
                onChangeText={(value) =>{
                  this.setState({monto: value});
                  this.validateDecimal(value);
                }
                }
                placeholder='Monto a pagar ej. 0000,00'
              />
              {
                this.state.errorMonto?
                  <Text style={{color:'#FF80AB', margin:0}}>
                  {this.state.errorMonto}
                  </Text>
                :null
              }
            </Item>
            </View>
            {
              this.state.checkVisible?
            <ListItem>
              <Body>
                <Text>Agregar contacto</Text>
              </Body>
            <Switch
                //onTintColor="#ffffff"
                thumbColor="#E60026"
                trackColor="#e8e8e8"
                style={{
                  flex: 1
                }}
                onValueChange = {this.handleCheckBox}
                value = {this.state.check}/>
            </ListItem>:null
            } 
            {
              this.state.check ==true?
              //<Label floatingLabel style={{borderColor: 'white',  marginLeft:20, marginRight:20,marginTop:10}}>Alias</Label>
              <Item>
              <Icon type="FontAwesome" name="user" style={{color:"#E60026"}}/>
                <Input 
                  keyboardType = 'default'
                  maxLength={20}
                  type='text' 
                  value={this.state.alias}
                  onChangeText={(value) => this.setState({alias: value})}
                  placeholder='Ingrese Nombre o Apodo'
                />
              </Item>:null
            }           
            {
              this.state.spinner == false?
              (
                <Button
                rounded
                style={{
                   flex: 1,
                   marginLeft:20,
                   marginRight:20,
                   marginTop:5,
                   borderColor:"#E60026",
                   backgroundColor:"#E60026"
                }}

                onPress={async () => {
                  
                  if (this.state.errorMonto != '') {
                    Alert.alert(
                        'Error Monto',
                        "¡El monto debe tener el formato requerido ej. 0000,00!",
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                      );
                  }else if (this.state.banco == '' || this.state.tlf == '' || this.state.cedula == '' || this.state.monto == '') {
                      Alert.alert(
                        'Aviso',
                        "¡Los campos no deben estar vacios!",
                        [
                          {text: 'OK', onPress: () => console.log('OK Pressed')},
                        ],
                        {cancelable: false},
                      );
                  }else {
                      Alert.alert(
                        'Aviso',
                        "¿Seguro quiere enviar este mensaje?: ",
                        [
                          {text: 'SI', onPress: async () => {
                             var mensaje = 'Pagar'+' '+this.state.banco+' '+this.state.tlf+' '+this.state.cedula+' '+this.state.monto;
                              await this.setState({
                                      spinner:true
                                    });
                              await this.enviarSms(mensaje);

                              if (this.state.check == true) {  
                                await this.saveUserId();
                              }
                              await this.getUserId();
                              await this.setState({
                                      banco:'',
                                      tlf:'',
                                      cedula:'',
                                      monto:'',
                                      alias:'',
                                      check:false,
                                      checkVisible:true,
                                      aliasVisible:false
                                    });

                          }},
                           {
                            text: 'NO',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                        ],
                        {cancelable: false},
                      )   
                  }

                }}
              >
              <Text style={{color:"white"}} >Enviar Pago</Text>
              <Icon type="FontAwesome" name="send"/>
              </Button>
              ):(
                <Spinner color='#E60026' /> 
              )}
          </Form>
        </Content>
          </Tab>
          <Tab heading="Operaciones" tabStyle={{backgroundColor: '#E60026'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#E60026'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}>
            <ViewSms />
          </Tab>
        </Tabs>
        <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Container>
            <Header style={{backgroundColor: "#E60026"}}>
            <Left>
              <Button
                  transparent
                  onPress={() => {
                    this.setState({
                      modalVisible:false
                    }) 
                  }}>
                  <Icon type="FontAwesome" name="arrow-left" />
                </Button>
               </Left> 
              <Body>
                <Title>Contactos Agregados</Title>
              </Body>
            </Header>
            <Content>

              <FlatList 
              data={store}
              renderItem={({item}) =>{
                return(
                  <View style={{borderColor: 'white',  marginLeft:10, marginRight:20,marginTop:5}}>
                  <ListItem
                    onPress = {async () => {

                    await this.setState({
                        banco:item.banco,
                        tlf:item.tlf,
                        cedula:item.cedula,
                        alias:item.alias,
                        modalVisible:false,
                        checkVisible:false,
                        aliasVisible:true
                        });

                    }}
                    onLongPress ={()=>{
                        Alert.alert(
                        'Aviso',
                        "¿Desea eliminar este contacto?",
                        [
                          {text: 'SI', onPress: async () => {
                            await this.deleteUserId(item.alias);
                            await this.getUserId();
                            await this.setState({
                              modalVisible:false
                              });
                          }},
                          {
                            text: 'NO',
                            onPress: () => console.log('Cancel Pressed'),
                            style: 'cancel',
                          },
                        ],
                        {cancelable: false},
                      );
                   }}
                    >
                    <Text>{item.alias}</Text>
                  </ListItem>
                  </View>
                  )
                   
              }}
              keyExtractor={(item, index) => index.toString()}>
              </FlatList>
            </Content>
          </Container>
          </Modal>
      </Container>  
    );
  }
}
